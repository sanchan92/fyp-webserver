<?php
$nameSelect = $_GET["path_name"];
$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, 'https://api.sanchan.space/hkcpn/getLocation.php?name='.$nameSelect);
$result = curl_exec($ch);
curl_close($ch);
include_once("config.inc.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <style>
    #map {
      height: 100%;
    }
    </style>
  </head>
  <body>

    <div id="map" style="height:100vh;"></div>
    <script>
    function initMap() {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: 'roadmap'
        };

        // Display a map on the web page
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        map.setTilt(50);

        <?php echo "var markers=$result;\n";?>

        // Add multiple markers to map
        var infoWindow = new google.maps.InfoWindow(), markers, i;

        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            infoWindow.setPosition(pos);
            infoWindow.setContent('Your current location.' + '</br> Latitude is ' + position.coords.latitude + '</br> Longitude' + position.coords.longitude);
            infoWindow.open(map);
            map.setCenter(pos);
          }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, infoWindow, map.getCenter());
        }
        // Place each marker on the map
        for (var key in markers){
          var position = new google.maps.LatLng(markers[key].latitude, markers[key].longitude);
          bounds.extend(position);
          marker = new google.maps.Marker({
              position: position,
              map: map,
              title: markers[key].location_mark
          });
        }

        for (var key in markers) {
        if (markers.hasOwnProperty(key)) {

              var position = new google.maps.LatLng(markers[key].latitude, markers[key].longitude);
              bounds.extend(position);
              marker = new google.maps.Marker({
                  position: position,
                  map: map,
                  title: markers[key].location_mark
              });

              // Add info window to marker
              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                  return function() {
                      infoWindow.setContent(markers[key].location_mark);
                      infoWindow.open(map, marker);
                  }
              })(marker, i));

              // Center the map to fit all markers on the screen
              map.fitBounds(bounds);

              }
        }

        // Set zoom level
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
            this.setZoom(14);
            google.maps.event.removeListener(boundsListener);
        });

    }
    // Load initialize function
    google.maps.event.addDomListener(window, 'load', initMap);
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=<?php echo $api_key; ?>&callback=initMap">
    </script>
  </body>
</html>
