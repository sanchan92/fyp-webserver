<?php
/*
This is used to get detailed infomation from database.
*/

include_once("database.inc.php");

if(isset($_GET["name"])){
  $isReady = true;
  $pathname = $_GET["name"];
}else{
  $isReady = false;
};


if ($isReady){
  $statement = $dbConn->prepare('Select location_mark,latitude,longitude from location_data where location_mark IN (Select mark_id from pathid where path_name = :name);');
  $statement->bindValue(':name', $pathname, PDO::PARAM_STR);
  $statement->execute();
  $result = $statement->fetchAll(PDO::FETCH_CLASS);

  if ( $result ) {
      echo json_encode($result, JSON_UNESCAPED_UNICODE);
      exit;
  } else {
      echo "No items found";
      exit;
  }
}else{
  echo "Access Denied";
}

 ?>
